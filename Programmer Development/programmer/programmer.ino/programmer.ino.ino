// pins 15~17 to GND, I2C bus address is 0x20
#include "Wire.h"

int output_byte = 0b00000000;
int debug = 0;

#define WE A1 //active low
#define OE A0 //active low
#define IO_0 2
#define IO_1 3
#define IO_2 4
#define IO_3 5
#define IO_4 6
#define IO_5 7
#define IO_6 8
#define IO_7 9

void mcp23017_output_dec(int output_number, bool state){
  int shifted = 0b00000000; 
  Wire.beginTransmission(0x20);

  if(output_number < 8){
    Wire.write(0x12); // GPIOA 
    shifted = 1 << (output_number - 1);
  }else{
    Wire.write(0x13); // GPIOB
    shifted = 1 << (output_number - 9);
  }
  if(state == HIGH){
      output_byte = output_byte | shifted;
  }else{
      shifted = ~shifted;
      output_byte = output_byte & shifted;
  } 
    
  Wire.write(output_byte); // port A
  Wire.endTransmission();
}

void mcp23017_output_bin(int gpioA_byte, int gpioB_byte){
  Wire.beginTransmission(0x20);
  Wire.write(0x12); // GPIOA   
  Wire.write(gpioA_byte); // port A
  Wire.endTransmission();
  
  Wire.beginTransmission(0x20);
  Wire.write(0x13); // GPIOB  
  Wire.write(gpioB_byte); // port B  
  Wire.endTransmission();
}

void waitSerial(){
  int x;
  if(debug == 1){
    while (!Serial.available()) 
    {
    }
    x = Serial.read();
  }
}

void print_binary(int v, int num_places)
{
    int mask=0, n;

    for (n=1; n<=num_places; n++)
    {
        mask = (mask << 1) | 0x0001;
    }
    v = v & mask;  // truncate v to specified number of places

    while(num_places)
    {

        if (v & (0x0001 << num_places-1))
        {
             Serial.print("1");
        }
        else
        {
             Serial.print("0");
        }

        --num_places;
    }
}

void setup()
{
   Serial.begin(115200);
   pinMode(OE, OUTPUT);
   pinMode(WE, OUTPUT);
    Wire.begin(); // wake up I2C bus
  // set I/O pins to outputs
   Wire.beginTransmission(0x20);
   Wire.write(0x00); // IODIRA register
   Wire.write(0x00); // set all of port A to outputs
   Wire.endTransmission();
   Wire.beginTransmission(0x20);
   Wire.write(0x01); // IODIRB register
   Wire.write(0x00); // set all of port B to outputs
   Wire.endTransmission();
}

void IO_inputs()
{
  int i;
  for(i=2;i<10;i++) //IO_0 of EEPROM is pin 2 on arduino, IO_8 is pin 9 on arduino 
  {
    pinMode(i, INPUT_PULLUP); 
  }
}

void IO_outputs()
{
  int i;
  for(i=2;i<10;i++) //IO_0 of EEPROM is pin 2 on arduino, IO_8 is pin 9 on arduino 
  {
    pinMode(i, OUTPUT); 
  }
}

void IO_read_byte(){
  int read_byte = 0x00;
  int i;

  IO_inputs();

    for(i=2;i<10;i++) //IO_0 of EEPROM is pin 2 on arduino, IO_8 is pin 9 on arduino 
    {
      if(digitalRead(i) == 1){
        read_byte |= 1 << (i-2); //when i = 2 if the pin is high then set bit 0 (2-2) to be 1
      }else{
        read_byte &= ~(1 << (i-2)); //when i = 2 if the pin is low then set bit 0 (2-2) to be 0
      } 
    }
    Serial.print(read_byte, HEX);
    Serial.print('\t');
}

void IO_write_byte(int output_byte){
  int i;
  int bit_state;
  
   digitalWrite(OE, HIGH);
   digitalWrite(WE, LOW);
   
  IO_outputs();
  for(i=0;i<8;i++) //IO_0 of EEPROM is pin 2 on arduino, IO_8 is pin 9 on arduino 
  {
    bit_state = (output_byte >> i) & 0x01;
    digitalWrite((i+2), bit_state);
  }

   digitalWrite(OE, LOW);
   digitalWrite(WE, HIGH);
   delay(10);   //this is due to the write time just pause a moment to allow write
}

void eeprom(char read_write, int address){
   int i,k;
   int read_position;
   float percentage_calc;

    if(read_write == 'b'){
      k = address/32;
      i = address - (k*32); 
      //if(debug == 1){
        Serial.print("k is:");
        Serial.println(k); 
        Serial.print("i is:");
        Serial.println(i);
      //}
      mcp23017_output_bin(i,k);
      IO_write_byte(0x00);
    }else{
       for(k=0;k<32;k++){//32
        if(debug == 1){
          Serial.print("Address is:");
          Serial.println(k,BIN);
        }
        for(i=0;i<256;i++){// put to 256
          if(debug == 1){
            Serial.print("i is:");
            Serial.println(i,BIN);
          }
          mcp23017_output_bin(i,k);
          if(read_write == 'w'){
            IO_write_byte(0xFE);
          }
          if(read_write == 'r'){
            if(debug == 1){
              Serial.print("read from EEPROM:");
            }
            if(i%8 == 0){ //after 8 reads do a new line
              Serial.println("");
              read_position = i+(256*k);
              Serial.print(read_position);
              Serial.print("\t");
            }
            IO_read_byte();
          }
         }
       }
    }
   Serial.println("");
   Serial.println("END OF EEPROM 8192 Words");
   Serial.println("");
}

char ReadSerial(){
  char incomingChar;
  while (!Serial.available()) 
  {
  }
  incomingChar = Serial.read(); // read the incoming byte:
  Serial.println(incomingChar);
  return incomingChar;
}

int ReadSerialInt(){
  int incomingInt;
  while (!Serial.available()) 
  {
  }
  incomingInt = Serial.parseInt();
  Serial.print("Writing to Byte: ");
  Serial.println(incomingInt);

  return incomingInt;
}

void loop(){
  char incomingChar;
  int incomingInt;
  Serial.println("Welcome to the EEPROM Programmer");
  Serial.println("");
  Serial.println("Press 'r' for Read, 'w' for Bulk Write, 'b' for Byte Write Mode, 'd' for Debug Mode");

  incomingChar = ReadSerial();

  switch(incomingChar){
    case 'r':
      Serial.println("You selected Read mode");
      eeprom('r', 0);
      break;
    case 'w':
      Serial.println("You selected Bulk Write mode");
      eeprom('w', 0);
      break;
    case 'b':
      Serial.println("You selected Byte Write mode");
      Serial.println("Which Address do you want to write to? 0-8192");
      incomingInt = ReadSerialInt();
      eeprom('b', incomingInt);
      break;
    case 'd':
      Serial.println("You selected Debug mode");
      debug = 1;
      break;
    default:
      break;
  }
}
