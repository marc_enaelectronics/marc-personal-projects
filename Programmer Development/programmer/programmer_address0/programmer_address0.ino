// pins 15~17 to GND, I2C bus address is 0x20
#include "Wire.h"

int output_byte = 0b00000000;

#define DEBUG_ON 1//1 is on

#define WE A1 //active low
#define OE A0 //active low
#define IO_0 2
#define IO_1 3
#define IO_2 4
#define IO_3 5
#define IO_4 6
#define IO_5 7
#define IO_6 8
#define IO_7 9

void waitSerial(){
  int x;
  if(DEBUG_ON == 1){
    while (!Serial.available()) 
    {
    }
    x = Serial.read();
  }
}

void print_binary(int v, int num_places)
{
    int mask=0, n;

    for (n=1; n<=num_places; n++)
    {
        mask = (mask << 1) | 0x0001;
    }
    v = v & mask;  // truncate v to specified number of places

    while(num_places)
    {

        if (v & (0x0001 << num_places-1))
        {
             Serial.print("1");
        }
        else
        {
             Serial.print("0");
        }

        --num_places;
    }
}

void setup()
{
 Serial.begin(9600);
 pinMode(OE, OUTPUT);
 pinMode(WE, OUTPUT);
}

void IO_inputs()
{
  int i;
  for(i=2;i<10;i++) //IO_0 of EEPROM is pin 2 on arduino, IO_8 is pin 9 on arduino 
  {
    pinMode(i, INPUT_PULLUP); 
  }
}

void IO_outputs()
{
  int i;
  for(i=2;i<10;i++) //IO_0 of EEPROM is pin 2 on arduino, IO_8 is pin 9 on arduino 
  {
    pinMode(i, OUTPUT); 
  }
}

void IO_read_byte(){
  int read_byte = 0x00;
  int i;
  
  IO_inputs();
  for(i=2;i<10;i++) //IO_0 of EEPROM is pin 2 on arduino, IO_8 is pin 9 on arduino 
  {
    if(digitalRead(i) == 1){
      read_byte |= 1 << (i-2); //when i = 2 if the pin is high then set bit 0 (2-2) to be 1
    }else{
      read_byte &= ~(1 << (i-2)); //when i = 2 if the pin is low then set bit 0 (2-2) to be 0
    } 
  }
  print_binary(read_byte, 8);
  Serial.println("");
}

void IO_write_byte(int output_byte){
  int i;
  int bit_state;
  
   digitalWrite(OE, HIGH);
   digitalWrite(WE, LOW);
   
  IO_outputs();
  for(i=0;i<8;i++) //IO_0 of EEPROM is pin 2 on arduino, IO_8 is pin 9 on arduino 
  {
    bit_state = (output_byte >> i) & 0x01;
    digitalWrite((i+2), bit_state);
  }

   digitalWrite(OE, LOW);
   digitalWrite(WE, HIGH);
   delay(10);   //this is due to the write time just pause a moment to allow write
}

void loop()
{
 int i;

  for(i=0;i<256;i++){
      IO_write_byte(i);
      IO_read_byte();
  }    
   waitSerial();
      /*
   IO_outputs();
   digitalWrite(OE, HIGH);
   digitalWrite(WE, LOW);
   digitalWrite(IO_0, HIGH);
   digitalWrite(OE, LOW);
   digitalWrite(WE, HIGH);
    
waitSerial();
   IO_inputs();
   Serial.println("AFTER WRITING IO_0 to 1");
   Serial.println(digitalRead(IO_0));

waitSerial();
   Serial.println("BEFORE WRITING");
   Serial.println(digitalRead(IO_0));
   
   IO_outputs();

   digitalWrite(OE, HIGH);
   digitalWrite(WE, LOW);
   digitalWrite(IO_0, LOW);
   digitalWrite(OE, LOW);
   digitalWrite(WE, HIGH);
      
waitSerial();
   IO_inputs();
   Serial.println("AFTER WRITING IO_0 to 0");
   Serial.println(digitalRead(IO_0));
   */
}
